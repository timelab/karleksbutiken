<?php
$site_url = get_site_url();
?>

<div class="wrap container mainText PodsBrands subpage" role="document">
    <div class="row relative">
        <div class="col-xs-12 subHeader">
            <?php get_template_part('templates/page', 'header'); ?>
        </div>
    </div>

    <div class="row relative minPageHeight">
        <div class="col-md-8 subContent">
            <?php if (!have_posts()) : ?>
                <div class="alert">
                    <?php _e('Sorry, no results were found.', 'roots'); ?>
                </div>
            <?php endif; ?>

            <?php while (have_posts()) : the_post(); ?>
                <?php get_template_part('templates/content', get_post_format()); ?>
            <?php endwhile; ?>

            <?php if ($wp_query->max_num_pages > 1) : ?>
                <nav class="post-nav">
                    <ul class="pager">
                        <li class="previous"><?php next_posts_link(__('&larr; Older posts', 'roots')); ?></li>
                        <li class="next"><?php previous_posts_link(__('Newer posts &rarr;', 'roots')); ?></li>
                    </ul>
                </nav>
            <?php endif; ?>
        </div>

        <div class="col-md-4 subBorderLeft">
            <ul class="blogsidebar">
                <?php
                dynamic_sidebar('Blog Sidebar');
                ?>
            </ul>
        </div>
    </div>
</div>
