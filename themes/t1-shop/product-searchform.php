<form role="search" method="get" id="searchform" action="<?php echo esc_url( home_url( '/'  ) ); ?>">
    <div>
        <input type="text" value="<?php echo get_search_query(); ?>" name="s" id="s" placeholder="<?php _e( 'Sökord', 'woocommerce' ); ?>" />
        <input type="submit" id="searchsubmit" value="<?php echo esc_attr__( 'Sök' ); ?>" />
    </div>
</form>