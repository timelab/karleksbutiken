<?php
/* Woocommerce specific filters / changes */


//Checkoutfields
/*
add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );
function custom_override_checkout_fields( $fields ) {

	return $fields;
}
*/

/**
 * Get the product thumbnail for the loop.
 */
function woocommerce_template_loop_product_thumbnail() {
    // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
    echo woocommerce_get_product_thumbnail('full');
}

function woocommerce_template_loop_product_title() {
    echo '<h3 class="' . esc_attr( apply_filters( 'woocommerce_product_loop_title_classes', 'woocommerce-loop-product__title' ) ) . '">' . get_the_title() . '</h3>'; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
}

function register_backorder_status() {
    register_post_status( 'wc-backorder', array(
        'label'                     => 'Restnoterade produkter',
        'public'                    => false,
        'exclude_from_search'       => false,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
        'label_count'               => _n_noop( 'Restnoterade ordrar <span class="count">(%s)</span>', 'Restnoterade ordrar <span class="count">(%s)</span>' )
    ) );
}
add_action( 'init', 'register_backorder_status' );

// Add to list of WC Order statuses
function add_backorder_to_order_statuses( $order_statuses ) {

    $new_order_statuses = array();

    // add new order status after processing
    foreach ( $order_statuses as $key => $status ) {

        $new_order_statuses[ $key ] = $status;

        if ( 'wc-processing' === $key ) {
            $new_order_statuses['wc-backorder'] = 'Restnoterade produkter';
        }
    }

    return $new_order_statuses;
}
add_filter( 'wc_order_statuses', 'add_backorder_to_order_statuses' );


function my_hide_shipping_when_free_is_available( $rates ) {

    if ( isset( $rates['free_shipping:2'] ) ){
        // To unset all methods except for free_shipping, do the following
        $free_shipping          = $rates['free_shipping:2'];
        $local_pickup           = $rates['legacy_local_delivery'];
        $rates                  = array();
        $rates['free_shipping:2'] = $free_shipping;
        $rates['legacy_local_delivery'] = $local_pickup;
    }
    return $rates;
}
add_filter( 'woocommerce_package_rates', 'my_hide_shipping_when_free_is_available', 100 );


function cart_notice() {
    global $wpdb, $woocommerce;

    $shipping = \WC_Shipping_Zones::get_zones();

    $maximum = intval(array_shift($shipping[3]['shipping_methods'])->min_amount);
    $current = WC()->cart->subtotal;
    if ( $current < $maximum ) {
        echo '<div class="woocommerce-info">Köp för ' . ($maximum - $current) . ' SEK till så får du fraktfri leverans.</div>';
    }
}
add_action( 'woocommerce_before_cart', 'cart_notice' );
add_action( 'woocommerce_before_checkout_form', 'cart_notice' );



add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
    add_theme_support( 'wc-product-gallery-lightbox' );
}

//Ta bort recensionstabben
add_filter( 'woocommerce_product_tabs', 'wcs_woo_remove_reviews_tab', 98 );
function wcs_woo_remove_reviews_tab($tabs) {
    unset($tabs['reviews']);
    return $tabs;
}

/* Dibs kräver 'SEK' istället för 'kr' */
add_filter('woocommerce_currency_symbol', 'add_my_currency_symbol', 10, 2);

function add_my_currency_symbol( $currency_symbol, $currency ) {
	//var_dump($currency);
     switch( $currency ) {
          case 'SEK': $currency_symbol = 'SEK'; break;
     }
     return $currency_symbol;
}


/*
 * Shipment tracking
 */

add_filter( 'wc_shipment_tracking_get_providers', 'shipping_providers');

function shipping_providers() {
	    return array(
        "Sverige" => array(
            'Postnord'     => 'http://www.postnord.se/sv/verktyg/sok/Sidor/spara-brev-paket-och-pall.aspx?search=%1$s',
            'DHL.se'        => 'http://www.dhl.se/content/se/sv/express/godssoekning.shtml?brand=DHL&AWB=%1$s',
            'Bring.se'      => 'http://tracking.bring.se/tracking.html?q=%1$s',
            'UPS.se'        => 'http://wwwapps.ups.com/WebTracking/track?track=yes&loc=sv_SE&trackNums=%1$s',
            'DB Schenker'   => 'http://privpakportal.schenker.nu/TrackAndTrace/packagesearch.aspx?packageId=%1$s'
        )
    );
}





/*
* Byta av bild saknas
*
**/
add_action( 'init', 'custom_fix_thumbnail' );

function custom_fix_thumbnail() {
	add_filter('woocommerce_placeholder_img_src', 'custom_woocommerce_placeholder_img_src');

	function custom_woocommerce_placeholder_img_src( $src ) {
		$src = get_template_directory_uri().'/assets/img/placeholder.png';
		return $src;
	}
}



//Products per page, 12 as standard
$productsPerPage = isset($_GET["visa"]) ? ($_GET["visa"] == 'alla' ? -1 : $_GET["visa"]) : 12;
add_filter( 'loop_shop_per_page', create_function( '$cols', 'return '. $productsPerPage .';' ), 20 );

add_action( 'pre_get_posts',  'set_posts_per_page'  );
function set_posts_per_page( $query ) {

  global $wp_the_query;

  $productsPerPage = isset($_GET["visa"]) ? ($_GET["visa"] == 'alla' ? -1 : $_GET["visa"]) : 12;

  if ( $query->is_search() ) {
    $query->set( 'posts_per_page', $productsPerPage );
  }

  return $query;
}


//New priceformat for variable products with "From 30 sek" insted of "30 sek to 60 sek"
add_filter( 'woocommerce_variable_sale_price_html', 'wc_wc20_variation_price_format', 10, 2 );
add_filter( 'woocommerce_variable_price_html', 'wc_wc20_variation_price_format', 10, 2 );
function wc_wc20_variation_price_format( $price, $product ) {

  $max_price = $product->get_variation_price( 'max', true );
  $min_price = $product->get_variation_price( 'min', true );
  $sale_price_min = $product->get_variation_sale_price('min',true);

  if($min_price != $product->get_variation_regular_price('min',true)){
    $min_price = $product->get_variation_regular_price('min',true);
  }





    if($sale_price_min == $min_price){


        if($max_price != $min_price){
            $price = '<ins>'.sprintf( __( 'Från: %1$s', 'woocommerce' ), wc_price( $min_price ) ).'</ins>';
        }else{
            $price = '<ins>'.wc_price( $min_price ).'</ins>';
        }

    }else {
        $regular_price_min = $product->get_variation_regular_price('min',true);


        if($max_price != $min_price){
            $price = '<del>'.wc_price( $regular_price_min ).'</del><ins>'.sprintf( __( ' Från: %1$s', 'woocommerce' ), wc_price( $sale_price_min ) ).'</ins>';
        }else{
            $price = '<del>'.wc_price( $regular_price_min ).'</del><ins>'.wc_price( $sale_price_min ).'</ins>';
        }
    }

    return $price;
}


// Ajax update for the cart-link, must be exactly the same HTML as the one in base.php
add_filter('add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment');
function woocommerce_header_add_to_cart_fragment( $fragments ) {
	global $woocommerce;

	get_the_ID();
	ob_start();
	?>
	<a class="cart-contents" href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="<?php _e('Visa kundkorg', 'woothemes'); ?>"><i class="fa fa-shopping-cart"></i><span><?php echo $woocommerce->cart->cart_contents_count; ?></span></a>
	<?php
	$fragments['a.cart-contents'] = ob_get_clean();
	return $fragments;
}


//New widget area for the webshop
register_sidebar(
	array (
		'name' => 'Shop Sidebar'
	)
);

//Widget area for startpage
register_sidebar(
    array (
        'name' => 'Product Carousel'
    )
);


//Read more link whereever it is used
function new_excerpt_more( $more ) {
	return '... <a class="read-more" href="'. get_permalink( get_the_ID() ) . '">Läs mer</a>';
}
add_filter( 'excerpt_more', 'new_excerpt_more' );


function woo_related_products_limit() {
	global $product;

	$args['posts_per_page'] = 3;
	return $args;
}
add_filter( 'woocommerce_output_related_products_args', 'jk_related_products_args' );
function jk_related_products_args( $args ) {

	$args['posts_per_page'] = 3; // 4 related products
	$args['columns'] = 3; // arranged in 4 columns
	return $args;
}

function woocommerce_store_thumbnail($size = 'shop_catalog', $placeholder_width = 0, $placeholder_height = 0) {
	global $post;
	$product = wc_get_product($post->ID);

	$images = $product->get_gallery_attachment_ids();

	if (!empty($images)) {
		echo wp_get_attachment_image( $images[0], apply_filters( $size, 'shop_catalog' ) );
	} else {
		// PLACEHOLDER HÄR
	}


}

// TEST SHIT
/*remove_action('woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail');
add_action('woocommerce_before_shop_loop_item_title', 'woocommerce_store_thumbnail', 10);*/
// Make Product "Featured" column sortable on Admin products list

/**
 * Filter products by type
 *
 * @access public
 * @return void
 */
function wpa104537_filter_products_by_featured_status() {

    global $typenow, $wp_query;
  
   if ($typenow=='product') :
  
      $info_taxonomy = get_taxonomy('product_visibility');
      $terms = get_terms(
        $info_taxonomy->name, // your custom taxonomy slug
          array(
              // get all the terms, even empty ones so we can see what items have 0
              'hide_empty' => false,
          )
      );
      $featuredCount = 0;
      foreach ($terms as $term) {
        if ( $term->slug != 'featured' )
          continue;
          
        $featuredCount = $term->count;
      }
  
       // Featured/ Not Featured
       $output .= "<select name='featured_status' id='dropdown_featured_status'>";
       $output .= '<option value="">Filtrera efter '.lcfirst($info_taxonomy->label).'</option>';
  
       $output .="<option value='featured' ";
       if ( isset( $_GET['featured_status'] ) ) $output .= selected('featured', $_GET['featured_status'], false);
       $output .=">".__( 'Featured', 'woocommerce' )." ($featuredCount)</option>";
  
       $output .="<option value='normal' ";
       $notFeaturedCount = array_sum((array)wp_count_posts( 'product' )) - $featuredCount;
       if ( isset( $_GET['featured_status'] ) ) $output .= selected('normal', $_GET['featured_status'], false);
       $output .=">Inte ".lcfirst(__( 'Featured', 'woocommerce' ))." ({$notFeaturedCount})</option>";
  
       $output .="</select>";
  
       echo $output;
   endif;
  }
  
  add_action('restrict_manage_posts', 'wpa104537_filter_products_by_featured_status');
  
  /**
   * Filter the products in admin based on options
   *
   * @access public
   * @param mixed $query
   * @return void
   */
  function wpa104537_featured_products_admin_filter_query( $query ) {
    global $typenow;
  
    if ( $typenow == 'product' ) {
  
        // Subtypes
        if ( ! empty( $_GET['featured_status'] ) ) {
            if ( $_GET['featured_status'] == 'featured' ) {
                $query->query_vars['tax_query'][] = array(
                    'taxonomy' => 'product_visibility',
                    'field'    => 'slug',
                    'terms'    => 'featured',
                );
            } elseif ( $_GET['featured_status'] == 'normal' ) {
                $query->query_vars['tax_query'][] = array(
                    'taxonomy' => 'product_visibility',
                    'field'    => 'slug',
                    'terms'    => 'featured',
                    'operator' => 'NOT IN',
                );
            }
        }
  
    }
  
  }
  add_filter( 'parse_query', 'wpa104537_featured_products_admin_filter_query' );



?>
