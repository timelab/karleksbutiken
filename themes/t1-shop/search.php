<?php
 global $query_string;
 global $wp_query, $paged;
 $classes[] = 'col-xs-6 col-sm-4 fullwidth';

 // $query_args = explode("&", $query_string);
 // $search_query = array(
 //                     'post_type' => 'product',
 //                     'posts_per_page' => -1
 //                 );
 // foreach($query_args as $key => $string) {
 //     $query_split = explode("=", $string);
 //     $search_query[$query_split[0]] = urldecode($query_split[1]);
 // } // foreach
 // $search_query['s'] = urldecode($search_query['s']);
 // $search_query['s'] = str_replace('&', '&amp;', $search_query['s']);

// $category = get_term_by('name', $search_query['s'], 'product_cat');


// $test_args = array(
//     'post_type'             => 'product',
//     'post_status'           => 'publish',
//     'posts_per_page'        => -1,
//     'meta_query'            => array(
//         array(
//             'key'           => '_visibility',
//             'value'         => array('catalog', 'visible'),
//             'compare'       => 'IN'
//         )
//     ),
//     'tax_query'                 => array(
//         array(
//             'taxonomy'      => 'product_cat',
//             'field'         => 'term_id', //This is optional, as it defaults to 'term_id'
//             'terms'         => $category->term_id,
//             'operator'      => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
//         )
//     )
// );

// $products = new WP_Query($test_args);
// $search = new WP_Query($search_query);

// $idArr = array();
 
// foreach($search->posts as $prod){
//     array_push($idArr, $prod->ID);
// }

// foreach ($products->posts as $prod2) {
//     if(!in_array($prod2->ID, $idArr)){
//         array_push($idArr, $prod2->ID);
//     }
// }

// $paged = get_query_var('paged') ? get_query_var('paged') : 1;

// // $finalargs = array(
// //   'post_type'      => 'product',
// //   'paged'          => $paged,
// //   'post__in'       => $idArr, 
// //   'posts_per_page' => 100
// // );
// // if(!empty($idArr)){
// //     $wp_query = new WP_Query($finalargs);
// // }
if ( $wp_query->is_search() ) {
  if(isset($_GET["visa"])){
    if($_GET["visa"] == 'alla'){
      $wp_query->max_num_pages = 1;
    }
  }
}
?>
<div class="wrap container mainText PodsBrands subpage" role="document">
    <div class="row relative">
        <div class="col-xs-12 subHeader">
            <div class="page-header">
                <h1>Sökresultat</h1>
            </div>
        </div>
    </div>

    <div class="row relative minPageHeight">
        <div class="col-sm-9 col-md-8 subContent">
            <?php do_action( 'woocommerce_before_shop_loop' ); ?>
            <div class="clearfix"></div>
            <div class="row products">
            <?php while (have_posts()){
                the_post(); ?>
                <div <?php post_class( $classes ); ?>>
                    <?php do_action( 'woocommerce_before_shop_loop_item' ); ?>

                    <a href="<?php the_permalink(); ?>">

                        <?php
                        /**
                         * woocommerce_before_shop_loop_item_title hook
                         *
                         * @hooked woocommerce_show_product_loop_sale_flash - 10
                         * @hooked woocommerce_template_loop_product_thumbnail - 10
                         */
                        do_action( 'woocommerce_before_shop_loop_item_title' );
                        ?>

                        <h3><?php the_title(); ?></h3>

                        <?php
                        /**
                         * woocommerce_after_shop_loop_item_title hook
                         *
                         * @hooked woocommerce_template_loop_rating - 5
                         * @hooked woocommerce_template_loop_price - 10
                         */
                        do_action( 'woocommerce_after_shop_loop_item_title' );
                        ?>

                    </a>

                    <?php do_action( 'woocommerce_after_shop_loop_item' ); ?>

                </div>
            <?php } ?>
            </div>
            <?php if ($wp_query->max_num_pages > 1) : ?>
                <?php do_action( 'woocommerce_after_shop_loop' ); ?>
            <?php endif; ?>
        </div>
        <?php wp_reset_postdata(); ?>
        <div class="col-sm-3 col-md-4 subBorderLeft shop-sidebar">
            <div class="view-btn"><i class="fa fa-spinner fa-spin"></i></div>
            <div class="row showcases collapsed">
                <div class="col-sm-12 shop-cat-menu">
                    <ul class="widget-ul">
                        <?php
                        dynamic_sidebar('Shop Sidebar');
                        ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>

</div>