<article <?php post_class(); ?>>
  <div class="row">
    <div class="col-sm-12">
      <header>
        <h4 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?> (Produkt)</a></h4>
      </header>
    </div>
  </div>
  <div class="row">
    <div class="col-xs-12 col-sm-2 text-center">
      <?php 
        if(has_post_thumbnail()){
          the_post_thumbnail('thumbnail');
        } else {
          echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<img src="%s" alt="%s" />', wc_placeholder_img_src(), __( 'Placeholder', 'woocommerce' ) ), $post->ID );
        }
      ?>
    </div>
    <div class="col-xs-12 col-sm-10">
      <div class="entry-summary">
        <?php the_excerpt(); ?>
      </div>
    </div>
  </div>
</article> 