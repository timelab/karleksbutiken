<?php
global $cms;
global $t1config;
?>

<div class="row top-header">
  <div class="col-xs-10 col-sm-8 logo-mobile">
    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/site-logo.png" alt="Logotype" class="topLogo"/>
  </div>

  <div class="col-xs-2 col-sm-4">
      <?php
      // Social networking icons.
      //--------------------------------------------------------------------
      $social = emitSocial($cms);
      if (!empty($social))
      {
        echo "<div class='social'>";
        echo $social;
        global $woocommerce;
        ?>
        <a class="cart-contents" href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="<?php _e('Visa kundkorg', 'woothemes'); ?>"><i class="fa fa-shopping-cart"></i><span><?php echo $woocommerce->cart->cart_contents_count;?></span></a>

        <?php

        echo "</div>";

      }
      //--------------------------------------------------------------------

      // Contact info.
      //--------------------------------------------------------------------
      $contactInfo = '';
      $repArr = array ("(0)", "-", " "); // array för att fixa till telefonnr i länkar.

      // Build HTML representation.
      $facilities = getContactInfo($cms);

      if ($t1config['header_type'] === HEADER_TYPE_1)
      {
        $contactInfo .= "<div class='item'>";
        foreach ($facilities as $facility)
        {
          if (isset($facility['email']))
          {
            $contactInfo .= "<div class='row topinfo-item'>";
            $contactInfo .= "<div class='col-xs-12'>";
            $contactInfo .= "<span class='glyphicon glyphicon-envelope'></span>";
            $contactInfo .= "<a href='mailto:{$facility['email']}'>{$facility['email']}</a>";
            $contactInfo .= "</div>";
            $contactInfo .= "</div>";
          }
          if (isset($facility['telephone']))
          {
            $contactInfo .= "<div class='row topinfo-item'>";
            $contactInfo .= "<div class='col-xs-12'>";
            $contactInfo .= "<span class='glyphicon glyphicon-phone'></span>";
            $contactInfo .= "<a href='tel:" . str_replace($repArr, '', $facility['telephone']) . "'>{$facility['telephone']}</a>";
            $contactInfo .= "</div>";
            $contactInfo .= "</div>";
          }
        }
        $contactInfo .= "</div>";
      }

      echo "<div class='top-info hidden-xs'>";
      echo $contactInfo;
      echo "</div>";
      //--------------------------------------------------------------------
      ?>
  </div>

</div>