<footer class="content-info container footer" role="contentinfo">
<div class="row">
    <!-- <div class="marginFixSub"> -->
            <div class="col-md-3 col-sm-6 ">
                <div class="col-sm-9 col-sm-offset-1  footerItem head ">Luleå</div>
                <div class="col-sm-9 col-sm-offset-1  footerItem  ">Grundvägen 93</div>
                <div class="col-sm-9 col-sm-offset-1  footerItem  ">Tel: 0920 - 23 11 93</div>
                <div class="col-sm-9 col-sm-offset-1  footerItem  ">Epost: info@dackcenter.se</div>
                <div class="col-sm-9 col-sm-offset-1  footerItem  ">Karta</div>
            </div>
            <div class="col-md-3 col-sm-6 ">
                <div class="col-sm-9 col-sm-offset-1  footerItem head">Boden</div>
                <div class="col-sm-9 col-sm-offset-1 footerItem  ">Garnisionsvägen 8</div>
                <div class="col-sm-9 col-sm-offset-1  footerItem  ">Tel: 0921 - 25 13 88</div>
                <div class="col-sm-9 col-sm-offset-1  footerItem  ">Epost: boden@dackcenter.se</div>
                <div class="col-sm-9 col-sm-offset-1  footerItem  ">Karta</div>
            </div>
            <div class="col-md-3 col-sm-6 ">
                <div class="col-sm-9 col-sm-offset-1 footerItem head ">Kalix</div>
                <div class="col-sm-9 col-sm-offset-1 footerItem  ">Nyborgsvägen 1</div>
                <div class="col-sm-9 col-sm-offset-1 footerItem  ">Tel: 0923 - 162 17</div>
                <div class="col-sm-9 col-sm-offset-1 footerItem  ">Epost: kalix@dackcenter.se</div>
                <div class="col-sm-9 col-sm-offset-1 footerItem  ">Karta</div>
            </div>
            <div class="col-md-3 col-sm-6 ">
                <div class="col-sm-9 col-sm-offset-1 footerItem head ">Haparanda</div>
                <div class="col-sm-9 col-sm-offset-1 footerItem  ">Norraindustrivägen 13</div>
                <div class="col-sm-9 col-sm-offset-1 footerItem  ">Tel: 0922 - 131 77</div>
                <div class="col-sm-9 col-sm-offset-1 footerItem  ">Epost: haparande@dackcenter.se</div>
                <div class="col-sm-9 col-sm-offset-1 footerItem  ">Karta</div>
            </div>
    <!-- </div> -->
</div>
</footer>

<?php wp_footer(); ?>