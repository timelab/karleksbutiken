<div class="wrap container mainText PodsBrands subpage" role="document">
  <div class="row relative">
    <div class="col-xs-12 subHeader">
      <?php get_template_part('templates/page', 'header'); ?>
    </div>
  </div>

  <div class="row relative minPageHeight">
    <div class="col-md-8 subContent">
      <?php while (have_posts()) : the_post(); ?>
        <article <?php post_class(); ?>>
          <div class="entry-content">
            <?php the_content(); ?>
          </div>
          <footer>
            <?php wp_link_pages(array('before' => '<nav class="page-nav"><p>' . __('Pages:', 'roots'), 'after' => '</p></nav>')); ?>
          </footer>
        </article>
      <?php endwhile; ?>
    </div>

    <div class="col-md-4 subBorderLeft">
      <ul class="blogsidebar">
      <?php
        dynamic_sidebar('Blog Sidebar');
      ?>
      </ul>
    </div>
  </div>
</div>


