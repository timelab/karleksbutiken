��          �      l      �     �     �            �        �               !  1   0     b  |   y     �       	          B   9  <   |     �     �  �  �     d  #   x     �     �  �   �     �     �     �     �  1   �     	  �   +	     �	     �	  
   �	     �	  N   �	  G   N
     �
     �
     
                                                                              	               %s (Error code: %s) Date of Birth Description Discount Enable Klarna Test Mode. This will only work if you have a Klarna test account. For test purchases with a live account, <a href="http://integration.klarna.com/en/testing/test-persons" target="_blank">follow these instructions</a>. Enable/Disable Gender Invoice Fee Klarna Invoice Klarna payment completed. Klarna Invoice number:  Klarna payment denied. Order is PENDING APPROVAL by Klarna. Please visit Klarna Online for the latest status on this order. Klarna Invoice number:  Payment plan TEST MODE ENABLED Test Mode Thank you for your order. This controls the description which the user sees during checkout. This controls the title which the user sees during checkout. Title Unknown response from Klarna. Project-Id-Version: WooCommerce Klarna Gateway v1.8.8
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-06-18 11:36+0100
PO-Revision-Date: 2015-06-18 11:36+0100
Last-Translator: Niklas Högefjord <info@krokedil.se>
Language-Team: 
Language: nl_NL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 1.7.6
X-Poedit-SourceCharset: utf-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Poedit-Basepath: ../
X-Textdomain-Support: yes
X-Poedit-SearchPath-0: .
 %s (Error code: %s) Geboortedatum invullen als DDMMYYYY Omschrijving Korting Activeer Klarna Test Modus. Dit gaat alleen werken indien u Klarna test account heeft. Voor het testen met een live account, <a href="http://integration.klarna.com/en/testing/test-persons" target="_blank">volg deze instructies op</a>. Activeer/Deactiveer Geslacht Administratiekosten Klarna Factuur Klarna betaling compleet. Klarna factuur nummer:  Klarna weigerde de betaling. De bestelling heeft status PENDING APPROVAL door Klarna. Ga naar Klarna om de status van deze order op te vragen. Klarna Factuur nummer:  Betalings plan TEST MODUS GEACTIVEERD Test Modus Dank voor uw bestelling. Hier kan je de omschrijving aanpassen wat de klant ziet tijdens het afrekenen. Hier kan je de titel aanpassen wat de klant ziet tijdens het afrekenen. Titel Onbekende reactie van Klarna. 