<?php

class brand_editor extends cms_module {

    public function __construct(&$cms) {
        add_action('admin_init', array($this, 'create_post_type'));
        add_action('admin_menu', array($this, 'create_brand_editor'));
    }

    function create_post_type() {
        $post_type_args =
            array(
                'labels' => array(
                    'name' => "Märken",
                    'singular_name' => "Märke"
                ),
                'description' => "Detta är ett märke",
                'public' => true,
                'exclude_from_search' => true,
                'supports' => array('title', 'custom-fields')
            );

        register_post_type('timelab_cms_brand', $post_type_args);
    }

    function create_brand_editor() {
        add_menu_page( 'Märken', 'Märken', 'edit_pages', 'timelab_cms_brands', array($this, 'output_brand_editor'), 'none', 17.1 );
    }

    function output_brand_editor() {
        $this->admin_page_template_top();

        $brands = get_posts(
            array(
                'post_type' => 'timelab_cms_brand',
                'posts_per_page' => -1,
                'meta_key' => 'order',
                'orderby' => 'meta_value_num',
                'order' => 'ASC'
            )
        );

        foreach ($brands as $brand) {
            echo "<h2>" . $brand->post_title . "</h2>";
        }

        $this->admin_page_template_bottom();
    }

} 