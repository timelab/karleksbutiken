<?php
namespace Timelab\Cms\Modules;


use Timelab\Cms\ApiInterface;

class WpmlApi implements ApiInterface {

    /**
     * Creates a copy of post in a new language.
     *
     * @param $id int The ID of the post to copy
     * @param $postType string The post's type
     * @param $language string The language code (ex: 'en')
     * @return int|\WP_Error
     */
    public function translatePost($id, $postType, $language) {
        $title = get_post($id)->post_title . ' (' . $language . ')';

        $translatedId = wp_insert_post(array(
            'post_title'    => $title,
            'post_type'     => $postType,
            'post_status'   => 'publish'
        ));

        global $sitepress;
        $defaultLanguage = $sitepress->get_default_language();

        global $wpdb;
        $wpdb->insert( $wpdb->prefix.'icl_translations',
            array (
                'trid' => $id,
                'element_id' => $translatedId,
                'element_type' => 'post_'.$postType,
                'language_code' => $language,
                'source_language_code' => $defaultLanguage
            ),
            array(
                '%d',
                '%d',
                '%s',
                '%s',
                '%s'
            )
        );

        return $translatedId;
    }

    /**
     * Get the languages currently setup in WPML
     * @return array
     */
    public function getLanguages() {
        return icl_get_languages('skip_missing=N');
    }

    /**
     * Gets the specified post's language
     * @param $id int The id of the post
     * @return array Language info for the specified post
     */
    public function getPostLanguage($id) {
        $language = apply_filters( 'wpml_post_language_details', NULL, $id );

        $langCode = array_shift(explode('_',$language['locale']));
        $language['country_flag_url'] = $this->getFlag($langCode);

        return $language;
    }

    /**
     * Gets all the translated versions of the post
     * @param $id int The id of the post to get
     * @param $postType string The post type of the post
     * @return array All the translated version of specified post
     */
    public function getTranslationsOfPost($id, $postType = null) {
        $languages = $this->getLanguages();
        global $wpdb;

        // Check if this post IS a translation of a post

        $language = $wpdb->get_results('SELECT trid FROM ' . $wpdb->prefix . 'icl_translations WHERE element_id = "' . $id . '"', ARRAY_A);

        if (!empty($language)) {
            $id = $language[0]['trid'];
        }

        $translations = array();
        foreach ($languages as $language) {

            $results = $wpdb->get_results('SELECT element_id FROM ' . $wpdb->prefix . 'icl_translations WHERE trid = "' . $id .
                '" AND language_code="' . $language['language_code'] . '"', ARRAY_A);

            if (!empty($results)) {
                $translations[] = array_merge($language, array('post_id' => $results[0]['element_id']));
            } else {
                $translations[] = array_merge($language, array('post_id' => null));
            }
        }

        return $translations;
    }

    /**
     * Get the post with the specified translation
     * @param $id int The id of the post to get
     * @param $langCode string The language code to get.
     * @return array The translated post, default language if not found.
     */
    public function getTranslationOfPost($id, $langCode) {
        $translations = $this->getTranslationsOfPost($id);

        $wpmlOptions = get_option( 'icl_sitepress_settings' );
        $defaultLang = $wpmlOptions['default_language'];

        $defaultPost = null;
        foreach($translations as $translation) {
            if ($translation['language_code'] == $langCode) {
                return $translation;
            }
            if ($translation['language_code'] == $defaultLang) {
                $defaultPost = $translation;
            }
        }

        return $defaultPost;
    }

    /**
     * Sets the post's language (By removing old WPML metakeys related to post)
     * @param $id int The ID of the post
     * @param $postType string The post's type
     * @param $language string The language code of the new language
     * @return int|null Returns ID if succesfull, `null` otherwise
     */
    public function setPostLanguage($id, $postType, $language) {
        // Delete prior language setting
        global $wpdb;
        $wpdb->delete($wpdb->prefix.'icl_translations',
            array(
                'element_id' => $id,
                'element_type' => 'post_'.$postType,
            ),
            array(
                '%d', '%d'
            )
        );


        global $sitepress;

        $parentId = icl_object_id($id, 'post_'.$postType, false, $sitepress->get_default_language());

        if (empty($parentId)) {
            $parentId = $id;
        }

        $wpdb->insert( $wpdb->prefix.'icl_translations',
            array (
                'trid' => $parentId,
                'element_id' => $id,
                'element_type' => 'post_'.$postType,
                'language_code' => $language,
                'source_language_code' => $sitepress->get_default_language()
            ),
            array(
                '%d',
                '%d',
                '%s',
                '%s',
                '%s'
            )
        );

        return $wpdb->insert_id;
    }

    /**
     * Gets the flag for a language
     * @param $languageCode string The code for the language
     * @return string Link to image
     */
    public function getFlag($languageCode) {
        return plugins_url() . '/sitepress-multilingual-cms/res/flags/' . $languageCode . '.png';
    }

} 