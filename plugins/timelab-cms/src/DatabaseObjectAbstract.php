<?php

namespace Timelab\Cms;


/**
 * A Database Object is an Object which is saved as a WP_Post.
 * Each Database Object comes with the standard WP Post fields like ID, post parent etc.
 *
 * When creating a new Database Object type you will probably want to override the save method, and update all
 * custom fields manually (After the base save method has done it's business
 *
 * Class DatabaseObjectAbstract
 * @package Timelab\Cms
 */
abstract class DatabaseObjectAbstract implements ObjectInterface {

    protected $id = null;
    private $title = null;
    private $slug = null;
    private $parentId = null;
    private $order = null;
    private $status = 'publish';
    /**
     * The post type of the object in the database, used internally by the object when saving.
     * Note: Post type can only be a maximum of 20 characters!
     * @return string The post type of the object
     */
    abstract public function getPostType();


    /**
     * Checks if user are allowed to save, or only administrators.
     * @return bool `true` if user can save, `false` if only admins can save the data.
     */
    abstract public function canUserSave();

    /**
     * Loads and sets data from a WP_Post on the object
     * @param $post /WP_Post the post to load from.
     * @throws \Exception
     */
    public function loadFromPost($post) {

        if ($post->post_type !== $this->getPostType()) {
            throw new \Exception("Trying to load post with another post type, aborted.");
        }

        $this->id = $post->ID;
        $this->setTitle($post->post_title);
        $this->setParentId($post->parent_id);
        $this->setStatus($post->post_status);
        $this->setOrder($post->menu_order);
        $this->setSlug($post->post_name);

        $this->loadFromPostFields();
    }

    /**
     * Runs after the loadFromPost method, used to get all custom_fields and other misc data from the database and apply
     * to the object.
     */
    abstract protected function loadFromPostFields();

    /**
     * Gets the ID of the object
     * @return int|null The ID if set, `null` if not set.
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Gets the title of the object
     * @return string The title
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Sets the title of the object
     * @param $title string The title
     */
    public function setTitle($title) {
        $this->title = $title;
    }

    /**
     * Gets the slug of the object
     * @return string The slug
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Sets the slug of the object
     * @param $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * Gets the ID of the parent to this object
     * @return int|null The ID of the parent, `null` if not set
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * Sets the parent of this object
     * @param int|null $parentId ID of the parent, `null` if none
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId;
    }

    /**
     * @return null
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param null $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }

    /**
     * Gets the status of the WP Post
     * @return string The status, possible statuses include: `publish`, `trash`, `private` and more.
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Sets the status of the WP Post
     * @param string $status The status, possible statuses include: `publish`, `trash`, `private` and more.
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Checks if the object is ready to be saved to the database, this is where all the validation lies.
     * @return bool `true` if object can be saved, `false` if not
     */
    abstract public function validateSave();

    /**
     * Saves the object to the database as a WP_Post, if ID is `null` it will create a post and set the ID of the object
     * @param $force bool Set to `true` if you want to ignore save validation
     * @throws \Exception
     */
    public function save($force = false) {

        if (!$force && !$this->validateSave()) {
            return;
        }

        if (!Cms::canUserEdit()) {
            return;
        }

        if (!$this->canUserSave() && !Cms::isSuperAdmin()) {
            return;
        }

        if ($this->getId() !== null && get_post_type($this->getId()) !== $this->getPostType()) {
            throw new \Exception("Post already exists and isn't the correct post type, aborting save.");
        }

        $post = array(
            'post_title'    => $this->getTitle(),
            'post_name'     => $this->getSlug(),
            'post_status'   => $this->getStatus(),
            'post_type'     => $this->getPostType(),
            'post_parent'   => $this->getParentId(),
            'menu_order'    => $this->getOrder()
        );

        // If slider has an ID, append it to the post so it gets updated (instead of creating a new one)
        if ($this->getId() !== null) {
            $post['ID'] = $this->getId();
            $newPost = false;
        } else {
            $newPost = true;
        }

        $this->id = wp_insert_post($post);

        if ($this->getId() === 0) {
            $this->id = null;
            throw new \Exception("Could not create new Post");
        }

        if ($newPost) {
            // If WPML module is loaded, set it to the default language.
            if (Cms::$instance->getModule('Wpml') !== null) {
                /** @var $wpmlApi \Timelab\Cms\Modules\WpmlApi */
                $wpmlApi = Cms::$instance->getApi('Wpml');

                global $sitepress;
                $wpmlApi->setPostLanguage($this->getId(), $this->getPostType(), $sitepress->get_default_language());
            }
        }

        $this->saveFields();
    }

    /**
     * Sets the status of the post to `trash`, therefore hiding it. If ID is null it simply sets the status to `trash`,
     * if ID is set it also automatically saves the object to the database.
     * @throws \Exception
     */
    public function trash() {

        $this->setStatus('trash');

        if ($this->getId() !== null) {
            $this->save(true);
        }
    }

    /**
     * Runs after the save method, used to save all custom_fields and other misc data to the database.
     */
    abstract protected function saveFields();

} 