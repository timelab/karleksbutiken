<?php

namespace Timelab\Cms\Objects;

use Timelab\Cms\DatabaseObjectAbstract;

class Person extends DatabaseObjectAbstract {

    private $name = null;
    private $role = null;
    private $department = null;
    private $description = null;

    /**
     * The post type of the object in the database, used internally by the object when saving.
     * Note: Post type can only be a maximum of 20 characters!
     * @return string The post type of the object
     */
    public function getPostType()
    {
        return 'timelab_cms_person';
    }

    /**
     * Checks if user are allowed to save, or only administrators.
     * @return bool `true` if user can save, `false` if only admins can save the data.
     */
    public function canUserSave()
    {
        return true;
    }

    /**
     * Runs after the loadFromPost method, used to get all custom_fields and other misc data from the database and apply
     * to the object.
     */
    protected function loadFromPostFields()
    {
        $this->setName(get_post_meta($this->getId(), 'name', true));
        $this->setRole(get_post_meta($this->getId(), 'role', true));
        $this->setDepartment(get_post_meta($this->getId(), 'department', true));
        $this->setDescription(get_post_meta($this->getId(), 'description', true));
    }

    /**
     * Checks if the object is ready to be saved to the database, this is where all the validation lies.
     * @return bool `true` if object can be saved, `false` if not
     */
    public function validateSave()
    {
        if ($this->getName() == (null||'')) {
            return false;
        }

        return true;
    }

    /**
     * Runs after the save method, used to save all custom_fields and other misc data to the database.
     */
    protected function saveFields()
    {
        update_post_meta($this->getId(), 'name', $this->getName());
        update_post_meta($this->getId(), 'role', $this->getRole());
        update_post_meta($this->getId(), 'department', $this->getDepartment());
        update_post_meta($this->getId(), 'description', $this->getDescription());
    }

    /**
     * @return mixed
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * @param mixed $department
     */
    public function setDepartment($department)
    {
        $this->department = $department;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param mixed $role
     */
    public function setRole($role)
    {
        $this->role = $role;
    }

    /**
     * Gets the associated image
     * @param $asArray bool `true` if you want to return Image as an associative array.
     * @return Image|array|null
     * @throws \Exception
     */
    public function getImage($asArray = false) {

        $image = new Image();

        $imagePosts = get_posts(array(
            'post_status'   => 'publish',
            'post_type'     => $image->getPostType(),
            'post_parent'   => $this->getId(),
            'posts_per_page'    => -1
        ));

        if (empty($imagePosts)) {
            if ($asArray) {
                return array();
            }

            return null;
        }

        $image->loadFromPost(get_post($imagePosts[0]));

        if ($asArray) {
            $image = $image->asArray();
        }

        return $image;
    }

    /**
     * Gets the contact details associated to person
     * @param bool $asArray
     * @return ContactDetail[]|array
     * @throws \Exception
     */
    public function getContactDetails($asArray = false) {

        $contactDetail = new ContactDetail();
        $posts = get_posts(array(
            'post_status'   => 'publish',
            'post_type'     => $contactDetail->getPostType(),
            'post_parent'   => $this->getId(),
            'orderby'       => 'menu_order',
            'order'         => 'ASC',
            'posts_per_page'    => -1
        ));

        $contactDetails = array();

        foreach ($posts as $post) {
            $contactDetail = new ContactDetail();
            $contactDetail->loadFromPost($post);

            if ($asArray) {
                $contactDetail = $contactDetail->asArray();
            }

            $contactDetails[] = $contactDetail;
        }

        return $contactDetails;
    }

    /**
     * Gets the first instance (or all) of the specified contact detail type.
     * @param $type ContactDetailType the type you want to get, if you want a phone number you enter `ContactDetailType::Telephone`
     * @return ContactDetail
     */
    public function getContactDetailByType($type) {
        $details = $this->getContactDetails();

        foreach($details as $detail) {
            if ($detail->getType() === $type) {
                return $detail;
            }
        }
    }

    /**
     * Gets all instances of the specified contact detail type.
     * @param $type ContactDetailType the type you want to get, if you want a phone number you enter `ContactDetailType::Telephone`
     * @return ContactDetail[]
     */

    public function getContactDetailsByType($type) {
        $details = $this->getContactDetails();
        $result = array();

        foreach($details as $detail) {
            if ($detail->getType() === $type) {
                array_push($result, $detail);
            }
        }

        return $result;
    }

    public function getTelephone() {
        return $this->getContactDetailByType(ContactDetailType::TELEPHONE);
    }

    public function getMobile() {
        return $this->getContactDetailByType(ContactDetailType::MOBILE);   
    }

    public function getFax() {
        return $this->getContactDetailByType(ContactDetailType::FAX);
    }

    public function getEmail() {
        return $this->getContactDetailByType(ContactDetailType::EMAIL);
    }

    public function asArray() {
        return array(
            'id'                => $this->getId(),
            'order'             => $this->getOrder(),
            'name'              => $this->getName(),
            'role'              => $this->getRole(),
            'department'        => $this->getDepartment(),
            'description'       => $this->getDescription(),
            'image'             => $this->getImage(true),
            'contact_details'   => $this->getContactDetails(true)
        );
    }
}