<?php

namespace Timelab\Cms\Objects;


use Timelab\Cms\DatabaseObjectAbstract;

class Facility extends DatabaseObjectAbstract {

    private $description;

    /**
     * The post type of the object in the database, used internally by the object when saving
     * @return string The post type of the object
     */
    public function getPostType()
    {
        return 'timelab_cms_facility';
    }

    /**
     * Checks if user are allowed to save, or only administrators.
     * @return bool `true` if user can save, `false` if only admins can save the data.
     */
    public function canUserSave()
    {
        return true;
    }

    /**
     * Checks if the object is ready to be saved to the database, this is where all the validation lies.
     * @return bool `true` if object can be saved, `false` if not
     */
    public function validateSave()
    {
        if ($this->getTitle() === null) {
            return false;
        }

        return true;
    }

    /**
     * Runs after the loadFromPost method, used to get all custom_fields and other misc data from the database and apply
     * to the object.
     */
    protected function loadFromPostFields()
    {
        $this->setOrder(get_post_meta($this->getId(), 'order', true));
        $this->setDescription(get_post_meta($this->getId(), 'description', true));
    }

    /**
     * Runs after the save method, used to save all custom_fields and other misc data to the database.
     */
    protected function saveFields()
    {
        update_post_meta($this->getId(), 'order', $this->getOrder());
        update_post_meta($this->getId(), 'description', $this->getDescription());
    }

    /**
     * Get the Facility's address
     * @param $asArray bool `true` if you want to return Address object as an associative array.
     * @return Address|array|null The Facility's address, `null` if facility has no address
     */
    public function getAddress($asArray = false) {
        $address = new Address();

        $addressPost = get_posts(
            array(
                'post_status'   => 'publish',
                'post_type'     => $address->getPostType(),
                'post_parent'   => $this->getId(),
                'orderby'       => 'menu_order',
                'posts_per_page'    => -1,
                'order' => 'ASC'
            )
        );

        if (!empty($addressPost)) {
            $address->loadFromPost($addressPost[0]);
        } else {
            return null;
        }

        if ($asArray) {
            $address = $address->asArray();
        }

        return $address;
    }

    public function getDescription() {
        return $this->description;
    }

    public function setDescription($text) {
        $this->description = $text;
    }

    /**
     * Gets the contact details associated with the facility.
     * @param $asArray bool `true` if you want to return all the ContactDetail objects as associative arrays.
     * @return ContactDetail[]|array[]
     * @throws \Exception
     */
    public function getContactDetails($asArray = false) {
        $contactDetail = new ContactDetail();

        $posts = get_posts(
            array(
                'post_status'       => 'publish',
                'post_type'         => $contactDetail->getPostType(),
                'post_parent'       => $this->getId(),
                'orderby'           => 'menu_order',
                'posts_per_page'    => -1,
                'order'             => 'ASC'
            )
        );

        $contactDetails = array();

        foreach ($posts as $post) {
            $contactDetail = new ContactDetail();
            $contactDetail->loadFromPost($post);

            if ($asArray) {
                $contactDetail = $contactDetail->asArray();
            }

            $contactDetails[] = $contactDetail;
        }

        return $contactDetails;
    }

    /**
     * Gets the first instance (or all) of the specified contact detail type.
     * @param $type ContactDetailType the type you want to get, if you want a phone number you enter `ContactDetailType::Telephone`
     * @return ContactDetail
     */
    public function getContactDetailByType($type) {
        $details = $this->getContactDetails();

        foreach($details as $detail) {
            if ($detail->getType() === $type) {
                return $detail;
            }
        }
    }

    /**
     * Gets all instances of the specified contact detail type.
     * @param $type ContactDetailType the type you want to get, if you want a phone number you enter `ContactDetailType::Telephone`
     * @return ContactDetail[]
     */

    public function getContactDetailsByType($type) {
        $details = $this->getContactDetails();
        $result = array();

        foreach($details as $detail) {
            if ($detail->getType() === $type) {
                array_push($result, $detail);
            }
        }

        return $result;
    }

    public function getTelephone() {
        return $this->getContactDetailByType(ContactDetailType::TELEPHONE);
    }

    public function getMobile() {
        return $this->getContactDetailByType(ContactDetailType::MOBILE);   
    }

    public function getFax() {
        return $this->getContactDetailByType(ContactDetailType::FAX);
    }

    public function getEmail() {
        return $this->getContactDetailByType(ContactDetailType::EMAIL);
    }

    /**
     * Gets the opening hours of the facility
     * @param $asArray bool `true' if you want to return all the opening hours as associative arrays.
     * @return TimeSpan[]|array[]
     * @throws \Exception
     */
    public function getOpeningHours($asArray = false) {
        $timeSpan = new TimeSpan();

        $posts = get_posts(
            array(
                'post_status'       => 'publish',
                'post_type'         => $timeSpan->getPostType(),
                'post_parent'       => $this->getId(),
                'posts_per_page'    => -1,
                'orderby'           => 'menu_order',
                'order'             => 'ASC'
            )
        );

        $openingHours = array();

        foreach ($posts as $post) {
            $openingHour = new TimeSpan();
            $openingHour->loadFromPost($post);

            if ($asArray) {
                $openingHour = $openingHour->asArray();
            }

            $openingHours[] = $openingHour;
        }

        return $openingHours;
    }

    /**
     * Get all of the facilities personnel grouped by their department.
     * @param $asArray bool `true` if you want to return personnel as associative arrays.
     * @return array
     */
    public function getDepartments($asArray = false) {
        $personnel = $this->getPersonnel();
        $departments = array();
        foreach ($personnel as $person) {
            if ($asArray) {
                $person = $person->asArray();
            }

            $departments[$person->getDepartment()][] = $person;
        }

        return $departments;
    }

    /**
     * Gets all of the facilities personnel.
     *
     * @param $asArray bool `true` if you want to return personnel as associative arrays.
     * @return Person[]|array
     * @throws \Exception
     */
    public function getPersonnel($asArray = false) {
        $person = new Person();

        $posts = get_posts(
            array(
                'post_status'       => 'publish',
                'post_type'         => $person->getPostType(),
                'posts_per_page'    => -1,
                'post_parent'       => $this->getId(),
                'orderby'           => 'menu_order',
                'order'             => 'ASC'
            )
        );

        $personnel = array();

        foreach ($posts as $post) {
            $person = new Person();
            $person->loadFromPost($post);

            if ($asArray) {
                $person = $person->asArray();
            }

            $personnel[] = $person;
        }

        return $personnel;
    }

    /**
     * Converts Facility to an associative array for use in template rendering
     * @return array The Facility as an array
     */
    public function asArray() {
        $address = $this->getAddress(true);

        return array (
            'id'                => $this->getId(),
            'title'             => $this->getTitle(),
            'status'            => $this->getStatus(),
            'parent_id'         => $this->getParentId(),
            'order'             => $this->getOrder(),
            'description'       => $this->getDescription(),
            'address'           => ($address !== null) ? $address : array(),
            'contact_details'   => $this->getContactDetails(true),
            'social_details'    => $this->getSocialDetails(true),
            'opening_hours'     => $this->getOpeningHours(true),
            'personnel'         => $this->getPersonnel(true)
        );
    }

    /**
     * Gets the social details associated with the facility.
     * @param $asArray bool `true` if you want to return all the SocialDetail objects as associative arrays.
     * @return SocialDetail[]|array[]
     * @throws \Exception
     */
    public function getSocialDetails($asArray = false) {
        $socialDetail = new SocialDetail();

        $posts = get_posts(
            array(
                'post_status'       => 'publish',
                'post_type'         => $socialDetail->getPostType(),
                'post_parent'       => $this->getId(),
                'orderby'           => 'menu_order',
                'posts_per_page'    => -1,
                'order'             => 'ASC'
            )
        );

        $socialDetails = array();

        foreach ($posts as $post) {
            $socialDetail = new SocialDetail();
            $socialDetail->loadFromPost($post);

            if ($asArray) {
                $socialDetail = $socialDetail->asArray();
            }

            $socialDetails[] = $socialDetail;
        }

        return $socialDetails;
    }

    /**
     * Gets the first instance (or all) of the specified social detail type.
     * @param $type SocialDetailType the type you want to get, if you want facebook link you enter `SocialDetailType::Facebook`
     * @return SocialDetail
     */
    public function getSocialDetailByType($type) {
        $details = $this->getSocialDetails();

        foreach($details as $detail) {
            if ($detail->getType() === $type) {
                return $detail;
            }
        }
    }

    /**
     * Gets all instances of the specified social detail type.
     * @param $type SocialDetailType the type you want to get, if you want a facebook link you enter `SocialDetailType::Facebook`
     * @return SocialDetail[]
     */

    public function getSocialDetailsByType($type) {
        $details = $this->getSocialDetails();
        $result = array();

        foreach($details as $detail) {
            if ($detail->getType() === $type) {
                array_push($result, $detail);
            }
        }

        return $result;
    }

    public function getFacebook() {
        return $this->getSocialDetailByType(SocialDetailType::FACEBOOK);
    }

    public function getInstagram() {
        return $this->getSocialDetailByType(SocialDetailType::INSTAGRAM);   
    }

    public function getLinkedin() {
        return $this->getSocialDetailByType(SocialDetailType::LINKEDIN);
    }

    public function getYoutube() {
        return $this->getSocialDetailByType(SocialDetailType::YOUTUBE);
    }
    public function getTwitter() {
        return $this->getSocialDetailByType(SocialDetailType::TWITTER);
    }

} 
