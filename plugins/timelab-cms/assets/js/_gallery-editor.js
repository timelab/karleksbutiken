function update_changed_images() {
    var images = $('.timelab_cms_gallery .thumbnail-image.changed'),
        html = "";

    images.each(function (index, image) {
        html += '<input type="text" name="image_id[]" value="' + $(image).data('id') + '"></input>' +
            '<input type="text" name="image_title[]" value="' + $(image).data('title') + '"></input>' +
            '<input type="text" name="image_tags[]" value="' + $(image).data('tags') + '"></input>' +
            '<input type="text" name="image_text[]" value="' + $(image).data('text') + '"></input>';
    });

    $('.timelab_cms_gallery #changed-images').html(html);
}

function update_removed_images() {
    var images = $('.timelab_cms_gallery .thumbnail-image.removed'),
        html = "";

    images.each(function (index, image) {
        html += '<input type="text" name="removed_image_id[]" value="' + $(image).data('id') + '"></input>';
    });

    $('.timelab_cms_gallery #removed-images').html(html);
}

function update_removed_tags() {

    var tags = $('.timelab_cms_gallery .tag-admin-list li.removed'),
        html = "";

    tags.each(function (index, tag) {
        html += '<input type="text" name="removed_tag_name[]" value="' + $(tag).data('tag-name') + '"></input>';
    });

    $('.timelab_cms_gallery #removed-tags').html(html);
}

function update_tag_buttons() {
    $('.timelab_cms_gallery .cancel-tag').unbind('click');
    $('.timelab_cms_gallery .cancel-tag').click(function () {
        $(this).closest('li').remove();
    });

    $('.timelab_cms_gallery .remove-tag').click(function () {
        $(this).closest('li').toggleClass('removed');
        update_removed_tags();
    });
}

function update_image_buttons() {
    $('.timelab_cms_gallery .thumbnail-image').click(function () {
        // Ladda in bild i editor
        $('#gallery-image-modal input[name="id"]').val($(this).data('id'));
        $('#gallery-image-modal input[name="title"]').val($(this).data('title'));
        $('#gallery-image-modal textarea[name="text"]').val($(this).data('text'));
        $('#gallery-image-preview').html($(this).html());
        $('#gallery-image-modal input[name="changed"]').val('false');

        $('#gallery-image-modal input[type="checkbox"]').attr('checked', false);

        if ($(this).data('removed') === true) {
            $('#gallery-image-modal #delete-image').html('Ångra Ta Bort');
        } else {
            $('#gallery-image-modal #delete-image').html('Ta Bort');
        }

        var tag_string = $(this).data('tags'),
            tags = tag_string.split("#");

        $.each(tags, function (i, element) {
            if (element.length > 0) {

                element = element.trim();

                $('#gallery-image-modal input[name="tags[]"][value="' + element + '"]').prop('checked', true);
            }
        });

        $('#gallery-image-modal').modal();
    });
}

$(function () {
    setTimeout(function () {
        // Bild admin
        update_image_buttons();

        $('.timelab_cms_gallery #gallery-image-modal #delete-image').click(function () {
            var id = $('#gallery-image-modal input[name="id"]').val(),
                image = $('.timelab_cms_gallery .thumbnail-image[data-id="' + id + '"]');

            $(image).data('removed', !$(image).data('removed'));
            $(image).toggleClass('removed');

            update_removed_images();
        });

        $('.timelab_cms_gallery #gallery-image-modal input, .timelab_cms_gallery #gallery-image-modal textarea').change(function () {
            $('#gallery-image-modal input[name="changed"]').val('true');
        });

        $('.timelab_cms_gallery #gallery-image-modal button[data-dismiss="modal"]').click(function () {

            if ($('#gallery-image-modal input[name="changed"]').val() === 'true') {
                // Spara
                var id = $('#gallery-image-modal input[name="id"]').val(),
                    image = $('.timelab_cms_gallery .thumbnail-image[data-id="' + id + '"]'),
                    tags = $('#gallery-image-modal input[type="checkbox"]:checked'),
                    tag_string = "";

                image.data('title', $('#gallery-image-modal input[name="title"]').val());
                image.data('text', $('#gallery-image-modal textarea[name="text"]').val());

                if (tags.length > 0) {
                    $(image).removeClass('no-tags');
                } else {
                    $(image).addClass('no-tags');
                }

                image.addClass('changed');

                tags.each(function (index, element) {
                    tag_string += " #" + $(element).val();
                });

                tag_string = tag_string.trim();

                image.data('tags', tag_string);

                update_changed_images();
            }
        });

        // Tagg admin
        $('.timelab_cms_gallery #add-tag-button').click(function () {
            var title = $('#new-tag-input').val();

            if (title.length <= 0) {
                return;
            }

            $('.tag-admin-list').append('<li> ' +
            '<div class="input-group"> ' +
            '<input type="text" class="form-control" value="' + title + '" name="new_tag[]"></input>' +
            '<span class="input-group-btn">' +
            '<button type="button" class="btn btn-danger cancel-tag">Ta bort</button>' +
            '</span>' +
            '</div></li>');

            $('#new-tag-input').val('');

            update_tag_buttons();
        });

        update_tag_buttons();

        // Bild uppladdning
        $('.timelab_cms_gallery #upload-image').click(function () {
            var image_upload_frame = create_media_frame('Galleribild', 'Lägg till foto', function(image) {
                var list = $('.timelab_cms_gallery #gallery-list'),
                    id = image.id,
                    src = image.url,
                    html = '<div class="' + $('#gallery-template').children().first().attr('class') + ' changed" data-id="' + id + '">' +
                        '<img src="' + src + '" />' +
                        '</div> ',
                    image_object;

                list.append(html);

                image_object = $('.timelab_cms_gallery #gallery-list .thumbnail-image[data-id="' + id + '"]');
                image_object.data('text', image.description);
                image_object.data('title', image.title);
                image_object.data('tags', image.caption);

                update_changed_images();
                update_image_buttons();

            }, true);

            image_upload_frame.open();
        });

        update_image_buttons();
    }, 0);
});