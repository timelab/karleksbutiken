function bind_facility_buttons() {
    $('.remove-facility').unbind();
    $('.remove-facility').click(function (e) {
        var $removed = $(this).siblings('input[name*=removed]');

        if ($removed.val() !== 'true') {
            $(this).siblings('input[name*=removed]').val('true').trigger('change');
            $(this).parent().addClass('removed');
            $(this).html("<span class='glyphicon glyphicon-trash'></span> Ångra ta bort");
        } else {
            $(this).siblings('input[name*=removed]').val('').trigger('change');
            $(this).parent().removeClass('removed');
            $(this).html("<span class='glyphicon glyphicon-trash'></span> Ta bort anläggning");
        }

        e.preventDefault();
    });
}

function add_new_facility() {
    $('#facility-editor-list').append(
        "<li class='puff'>" +
            "<div>" +
            "<span class='glyphicon glyphicon-home'></span> " +
            "<input type='text' name='name[]' value=''></input>" +
            "<input type='hidden' name='order[]' value='" + $('input[name*=order]').length + "'></input>" +
            "<input type='hidden' name='id[]' value=''></input>" +
            "<input type='hidden' name='removed[]' value=''></input> " +
            "<a href='#' class='btn btn-sm btn-danger remove-facility'><span class='glyphicon glyphicon-trash'></span> Ta bort anläggning</a>" +
            "</div>" +
            "</li>"
    );

    bind_facility_buttons();
    bindDirtyChecker();
}

function get_address_info(address, callback_function) {
    $.getJSON("http://maps.googleapis.com/maps/api/geocode/json?address=" + address + "&sensor=false&language=sv&region=se", function(data) {

        if (data.status === 'ZERO_RESULTS') {
            return;
        }

        if (data.status !== "OK") {
            console.warn('Google API rapporterade status: ' + data.status);
        }

        var results = {};

        results.total = data.results.length;
        results.formatted = data.results[0].formatted_address;
        results.location = data.results[0].geometry.location;


        $.each(data.results[0].address_components, function(key, value) {

            $.each(value.types, function(type_key, type_value) {
                switch (type_value) {
                    case 'postal_town':
                        results.city = value.long_name;
                        break;
                    case 'street_number':
                        results.street_number = value.long_name;
                        break;
                    case 'route':
                        results.street = value.long_name;
                        break;
                    case 'postal_code':
                        results.zip = value.long_name;
                        break;
                }
            });

        });

        callback_function(results);
    });
}

function initialize_map(lat, long, id) {
    var position = new google.maps.LatLng(lat, long);

    var mapOptions = {
        scrollwheel: false,
        zoom: 11,
        center: position,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };


    var styles = [
        {
            "featureType": "water",
            "stylers": [
                { "color": "#6e9fd7" }
            ]
        },{
        }
    ];

    var styledMap = new google.maps.StyledMapType(styles,
        {name: "Styled Map"});

    var map = new google.maps.Map(document.getElementById(id), mapOptions);

    map.mapTypes.set('map_style', styledMap);
    map.setMapTypeId('map_style');

    var marker = new google.maps.Marker({position: position,map: map});

   // console.log($('#' + id).parents('a.panel-heading'));

    $('#facility-accordion a.panel-heading').click(function () {
        setTimeout(function () {
            google.maps.event.trigger(map, "resize");
            map.setCenter(position);
        }, 100);
    });
}




function bind_opening_hours_remove() {
    $('#oppettider .remove-button').unbind('click');
    $('#oppettider .remove-button').click(function (e) {

        e.preventDefault();

        var remove = $(this).siblings('input[name*="opening_hours_remove"]')[0];

        if ($(remove).val() !== 'true') {
            $(remove).val('true');
            $(this).closest('li').addClass('removed');
        } else {
            $(remove).val('');
            $(this).closest('li').removeClass('removed');
        }

    });
}
// TODO: Kasta ihop set_template till en generic function (DRY)
function opening_hours() {
    var opening_hours_template;

    function set_template( $template ) {

        opening_hours_template = $template.parent().html();

        $template.change(function () {
            $(this).removeClass('placeholder');
            $(this).parent().removeClass('sort-disabled');
            $(this).find('input[name*=order]').val($('#opening-hours-list li').length-1);

            $('#opening-hours-list').append("<li class='sort-disabled'>" + opening_hours_template + "</li>");

            set_template($('#opening-hours-list .placeholder'));

            $template.unbind('change');
        });


    }

    set_template($('#opening-hours-list .placeholder'));

    bind_opening_hours_remove();
}

var image_upload_frame;

function personnel_update_buttons() {

    var current_person = null;

    $('.person-image-upload').unbind('click');
    $('.person-remove-button').unbind('click');

    $('.person-image-upload').click(function () {
        current_person = $(this).closest('.person');

        image_upload_frame = create_media_frame('Personalbild', 'Lägg till foto', function(image) {
            current_person.find('input[name*="person_image_mediaId"]').val(image.id);
            current_person.find('.person_image').attr('src', image.url);
        });

        image_upload_frame.close();
        image_upload_frame.open();
    });

    $('.person-remove-button').click(function (e) {
        e.preventDefault();

        // TODO: Gör det bättre än parent*4
        var parent = $(this).parent().parent().parent();

        if (parent.hasClass('placeholder')) { return; }

        if (parent.children('input[name*="person_remove"]').val() === 'true') {
            parent.children('input[name*="person_remove"]').val('');
            parent.removeClass('removed');
        } else {
            parent.children('input[name*="person_remove"]').val('true');
            parent.addClass('removed');
        }
    });
}

function personnel() {
    var personnel_template = null,
        current_person = null;

    function set_personnel_template( $template ) {
        if ($template.length === 0) {
            return;
        }

        personnel_template = $template.parent().html().format();

        $template.change(function () {
            if (!$(this).hasClass('placeholder')) { return; }
            $(this).removeClass('placeholder');
            $(this).parent().removeClass('sort-disabled');
            $(this).find('input[name*=order]').val($('#personnel-list > li').length-1);

            $('#personnel-list').append("<li class='sort-disabled'>" + personnel_template + "</li>");

            set_personnel_template($('#personnel-list .placeholder.person'));

            $template.unbind('change');

            personnel_update_buttons();
        });
    }

    // Personal
    set_personnel_template($('#personnel-list .placeholder.person'));

    personnel_update_buttons();
}

$(function () {


    // Gör så att enter INTE submittar formuläret när man trycker på enter
    $(window).keydown(function(event){
        if(event.keyCode === 13) {
            if ($("input[name*='address_search']").is(':focus')) {
                $('.address-search-button').trigger('click');

                event.preventDefault();
                return false;
            }
        }
    });

    bind_facility_buttons();

    $('#new-facility-button').click(function() {
        add_new_facility();
    });

    // Gatuadress
    $('.address-search-button').click(function() {
        get_address_info($('input[name*=address_search]').val(), function(address) {
            $('input[name*=address_formatted]').val(address.formatted).trigger('change');
            $('input[name*=address_street]').val(address.street + " " + address.street_number).trigger('change');
            $('input[name*=address_zip]').val(address.zip).trigger('change');
            $('input[name*=address_city]').val(address.city).trigger('change');
            $('input[name*=address_lat]').val(address.location.lat).trigger('change');
            $('input[name*=address_long]').val(address.location.lng).trigger('change');

            // Skapa google maps karta
            initialize_map(address.location.lat, address.location.lng, 'address-map-container');
        });
    });

    $('input[name*=address_lat]').each(function() {
        var lat = $(this).val(),
            long = $('input[name*=address_long]').val();

        if (lat === '0' && long === '0') {
            return;
        }


        initialize_map(lat, long, 'address-map-container');
    });

    if (document.URL.indexOf('timelab_cms_facility') >= 0 || document.URL.indexOf('timelab_cms_Contact')) {

        setTimeout(function() {
            // Öppettider
            opening_hours();

            // Personal
            personnel();
        }, 0);

    }
});