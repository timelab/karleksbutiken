function bindModuleCheckboxes() {
    // When you click the checkbox of a module, update the list of running modules and set the value of the settings input
    $('input.module-active').change(function () {
        var loaded_modules = "";

        $('input.module-active').each(function () {
            if ($(this).is(":checked")) {
                loaded_modules += $(this).data('module_name') + ",";
            }
        });

        loaded_modules = loaded_modules.replace(/(^,)|(,$)/g, "");

        $('input[name="timelab_Options_running_modules"]').val(loaded_modules);
    });
}

$(function () {
    setTimeout(bindModuleCheckboxes, 0);
});